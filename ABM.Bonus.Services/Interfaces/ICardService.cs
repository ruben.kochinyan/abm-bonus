﻿using ABM.Bonus.Entities;
using System.Threading.Tasks;

namespace ABM.Bonus.Services.Interfaces
{
    public interface ICardService : IService<Card>
    {
        Task<Card> GetByBarCodeAsync(string barCode);
        Task<Card> GetByUserIdAsync(int userId);
    }
}
