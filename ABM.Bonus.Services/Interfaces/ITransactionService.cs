﻿using ABM.Bonus.Entities;
using ABM.Bonus.Enums;
using ABM.Bonus.Models.Transaction;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ABM.Bonus.Services.Interfaces
{
    public interface ITransactionService : IService<TransactionFact>
    {
        Task<IEnumerable<TransactionFact>> GetByUserIdAsync(int userId);
        Task<IEnumerable<TransactionFact>> GetByBarCodeAsync(string barCode);
        Task<IEnumerable<TransactionFact>> Filter(TransactionFilterRequestModel model);
        Task Insert(Card card, decimal bonus, TransactionType transactionType);
    }
}
