﻿using ABM.Bonus.Entities;

namespace ABM.Bonus.Services.Interfaces
{
    public interface IPartnerService : IService<Partner>
    {
    }
}
