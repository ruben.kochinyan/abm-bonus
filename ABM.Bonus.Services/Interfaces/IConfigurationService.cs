﻿using ABM.Bonus.Models.Options;

namespace ABM.Bonus.Services.Interfaces
{
    public interface IConfigurationService
    {
        ControllerTestResults ControllerTestResultsConfiguration();
        UrlForTests UrlForTestsConfiguuration();
        ControllerNames ControllerNamesConfiguration();
        ActionNames ActionNamesConfiguration();
    }
}
