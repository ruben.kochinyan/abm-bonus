﻿using ABM.Bonus.DAL;
using ABM.Bonus.Entities;
using ABM.Bonus.Services.Interfaces;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;

namespace ABM.Bonus.Services.Classes
{
    public class CardService : Service<Card>, ICardService
    {
        public CardService(IRepository<Card> repository) : base(repository)
        {
        }

        public async Task<Card> GetByBarCodeAsync(string barCode)
        {
            return await Repository
                .Filter(c => c.BarCode == barCode)
                .FirstOrDefaultAsync();
        }

        public async Task<Card> GetByUserIdAsync(int userId)
        {
            var result = await Repository
                .Filter(c => c.UserId.Value == userId)
                .FirstOrDefaultAsync();

            return result;
        }
    }
}
