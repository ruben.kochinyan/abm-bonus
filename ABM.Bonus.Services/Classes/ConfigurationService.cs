﻿using ABM.Bonus.Models.Options;
using ABM.Bonus.Services.Interfaces;
using Microsoft.Extensions.Options;

namespace ABM.Bonus.Services.Classes
{

    public class ConfigurationService : IConfigurationService
    {
        private readonly IOptions<ControllerTestResults> testResults;
        private readonly IOptions<UrlForTests> url;
        private readonly IOptions<ControllerNames> controllerNames;
        private readonly IOptions<ActionNames> actioNames;

        public ConfigurationService(IOptions<ControllerTestResults> testResults,
            IOptions<UrlForTests> url,
            IOptions<ControllerNames> controllerNames,
            IOptions<ActionNames> actioNames)
        {
            this.testResults = testResults;
            this.url = url;
            this.controllerNames = controllerNames;
            this.actioNames = actioNames;
        }

        public ControllerTestResults ControllerTestResultsConfiguration() => testResults.Value;

        public UrlForTests UrlForTestsConfiguuration() => url.Value;

        public ControllerNames ControllerNamesConfiguration() => controllerNames.Value;

        public ActionNames ActionNamesConfiguration() => actioNames.Value;
    }
}
