﻿using ABM.Bonus.DAL;
using ABM.Bonus.Entities;
using ABM.Bonus.Enums;
using ABM.Bonus.Models.Transaction;
using ABM.Bonus.Services.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ABM.Bonus.Services.Classes
{
    public class TransactionService : Service<TransactionFact>, ITransactionService
    {
        public TransactionService(IRepository<TransactionFact> repository) : base(repository)
        {
        }

        public async Task<IEnumerable<TransactionFact>> GetByUserIdAsync(int userId)
        {
            return await Repository
                .Filter(t => t.UserId == userId)
                .ToListAsync();
        }

        public async Task<IEnumerable<TransactionFact>> GetByBarCodeAsync(string barCode)
        {
            return await Repository
                .Filter(t => t.CardBarCode == barCode)
                .ToListAsync();
        }

        public async Task Insert(Card card, decimal bonus, TransactionType transactionType)
        {
            await Repository
                .AddAsync(new TransactionFact
                {
                    Bonus = bonus,
                    CardBarCode = card.BarCode,
                    PartnerId = card.PartnerId,
                    UserId = card.UserId.HasValue ? card.UserId.Value : int.MaxValue,
                    TransactionType = transactionType
                });
        }

        public async Task<IEnumerable<TransactionFact>> Filter(TransactionFilterRequestModel model)
        {
            List<TransactionFact> facts = new List<TransactionFact>();

            if (model.UserId.HasValue)
            {
                facts.AddRange(await Repository.Filter(t => t.UserId == model.UserId.Value).ToListAsync());
            }
            else
            {
                facts.AddRange(await Repository.SelectAll().ToListAsync());
            }

            facts = FilterByBonus(facts, model.BonusFrom, model.BonusTo);

            facts = FilterByConcreteDay(facts, model.ConcreteDay);

            facts = FilterByDate(facts, model.From, model.To);

            return facts;
        }

        private List<TransactionFact> FilterByBonus(List<TransactionFact> facts, decimal? bonusFrom, decimal? bonusTo)
        {
            if (bonusFrom.HasValue && bonusTo.HasValue)
            {
                facts = facts.Where(f => f.Bonus >= bonusFrom.Value && f.Bonus <= bonusTo.Value).ToList();
            }
            else
            {
                if (bonusFrom.HasValue)
                {
                    facts = facts.Where(f => f.Bonus >= bonusFrom.Value).ToList();
                }
                if (bonusTo.HasValue)
                {
                    facts = facts.Where(f => f.Bonus <= bonusTo.Value).ToList();
                }
            }

            return facts;
        }

        private List<TransactionFact> FilterByConcreteDay(List<TransactionFact> facts, DateTime? day)
        {
            if (day.HasValue)
                facts = facts.Where(f => f.CreatedDate.Day == day.Value.Day).ToList();
            return facts;
        }

        private List<TransactionFact> FilterByDate(List<TransactionFact> facts, DateTime? from, DateTime? to)
        {
            if (from.HasValue && to.HasValue)
            {
                facts = facts.Where(f => f.CreatedDate >= from.Value && f.CreatedDate <= to.Value).ToList();
            }
            else
            {
                if (from.HasValue)
                {
                    facts = facts.Where(f => f.CreatedDate >= from.Value).ToList();
                }
                if (to.HasValue)
                {
                    facts = facts.Where(f => f.CreatedDate <= to.Value).ToList();
                }
            }

            return facts;
        }
    }
}
