﻿using ABM.Bonus.DAL;
using ABM.Bonus.Entities;
using ABM.Bonus.Services.Interfaces;

namespace ABM.Bonus.Services.Classes
{
    public class UserService : Service<User>, IUserService
    {
        public UserService(IRepository<User> repository) : base(repository)
        {
        }
    }
}
