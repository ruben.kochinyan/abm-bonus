﻿using ABM.Bonus.DAL;
using ABM.Bonus.Entities;
using ABM.Bonus.Services.Interfaces;

namespace ABM.Bonus.Services.Classes
{
    public class PartnerService : Service<Partner>, IPartnerService
    {
        public PartnerService(IRepository<Partner> repository) : base(repository)
        {
        }
    }
}
