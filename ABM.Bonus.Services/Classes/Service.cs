﻿using ABM.Bonus.DAL;
using ABM.Bonus.Entities;
using ABM.Bonus.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace ABM.Bonus.Services.Classes
{
    public class Service<TEntity> : IService<TEntity> 
        where TEntity : EntityBase
    {
        protected IRepository<TEntity> Repository { get; private set; }

        public Service(IRepository<TEntity> repository)
        {
            Repository = repository;
        }

        public void Add(TEntity entity) => Repository.Add(entity);

        public async Task AddAsync(TEntity entity) => await Repository.AddAsync(entity);

        public void AddRange(IEnumerable<TEntity> entities) => Repository.AddRange(entities);

        public async Task AddRangeAsync(IEnumerable<TEntity> entities) => await Repository.AddRangeAsync(entities);

        public void Update(TEntity entity) => Repository.Update(entity);

        public void UpdateRange(IEnumerable<TEntity> entities) => Repository.UpdateRange(entities);

        public TEntity Find(object id) => Repository.Find(id);
    }
}
