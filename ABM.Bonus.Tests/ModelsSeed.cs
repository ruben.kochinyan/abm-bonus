﻿using ABM.Bonus.Entities;
using ABM.Bonus.Models.Bonus;
using ABM.Bonus.Models.Card;
using ABM.Bonus.Models.Core;
using ABM.Bonus.Models.Options;
using ABM.Bonus.Models.Partner;
using ABM.Bonus.Models.Transaction;
using System;
using System.Collections.Generic;

namespace ABM.Bonus.Tests
{
    internal static class ModelsSeed
    {
        internal static TransactionFilterRequestModel TransactionFilterRequestModel(int? userId, DateTime? from, 
            DateTime? to, DateTime? concreteDay, decimal? bonusFrom, decimal? bonusTo)
        {
            return new TransactionFilterRequestModel
            {
                UserId = userId,
                BonusFrom = bonusFrom,
                BonusTo = bonusTo,
                ConcreteDay = concreteDay,
                From = from,
                To = to
            };
        }

        internal static AddBonusRequestModel AddBonusRequestModel(string barCode, decimal bonus)
        {
            return new AddBonusRequestModel
            {
                BarCode = barCode,
                Bonus = bonus
            };
        }

        internal static BonusOutRequestModel BonusOutRequestModel(string barCode, decimal bonus)
        {
            return new BonusOutRequestModel
            {
                BarCode = barCode,
                Bonus = bonus
            };
        }

        internal static AddCardRequestModel AddCardRequestModel(string barCode, decimal bonus, DateTime? expireDate)
        {
            return new AddCardRequestModel
            {
                BarCode = barCode,
                Bonus = bonus,
                ExpireDate = expireDate
            };
        }

        internal static AssignRequestModel AssignRequestModel(string barCode, string firstName, string lastName, string phoneNumber, string pasportSerialnumber)
        {
            return new AssignRequestModel
            {
                BarCode = barCode,
                FirstName = firstName,
                LastName = lastName,
                PassportSerialNumber = pasportSerialnumber,
                PhoneNumber = phoneNumber
            };
        }

        internal static AddPartnerRequestModel AddPartnerRequestModel(string name, string email, string phoneNumber)
        {
            return new AddPartnerRequestModel
            {
                Email = email,
                Name = name,
                PhoneNumber = phoneNumber
            };
        }

        internal static List<TransactionFact> GetListOfTransactionFacts() => new List<TransactionFact>();

        internal static ApiResponseModel<TModel> GetApiResponseModel<TModel>(TModel data)  => new ApiResponseModel<TModel>(data);

        internal static CardResponseModel GetCardResponseModel() => new CardResponseModel();

        internal static Card GetCard() => new Card();

        internal static ControllerTestResults GetControllerTestResults()
        {
            return new ControllerTestResults
            {
                SuccessResultWithoutModel = "\"{\"success\":true,\"statusCode\":200,\"data\":true}\"",
                BonusAddInvalidResult = "{\"success\":false,\"statusCode\":200,\"data\":[{\"key\":\"Error\",\"message\":\"'Bonus' must not be empty.\"}," +
                "{\"key\":\"Error\",\"message\":\"'Bar Code' must not be empty.\"}]}",
                BonusOutInvalidResult = "{\"success\":false,\"statusCode\":200,\"data\":[{\"key\":\"Error\",\"message\":\"'Bar Code' must not be empty.\"}]}",
                BonusOutNotEnoughBalanceResult = "{\"success\":false,\"statusCode\":200,\"data\":[{\"key\":\"Error\",\"message\":\"Not enough balance!\"}]}",
                CardAddInvalidResult = "{\"success\":false,\"statusCode\":200,\"data\":[{\"key\":\"Error\",\"message\":\"'Bonus' must not be empty.\"}]}",
                CardAssignInvalidResult = "{\"success\":false,\"statusCode\":200,\"data\":[{\"key\":\"Error\",\"message\":\"'Bar Code' must not be empty.\"}," +
                "{\"key\":\"Error\",\"message\":\"'First Name' must not be empty.\"},{\"key\":\"Error\",\"message\":\"'Passport Serial Number' must not be empty.\"}]}",
                CardGetSuccessResult = "{\"success\":true,\"statusCode\":200,\"data\":{\"bonus\":12.00,\"expireDate\":null,\"userId\":6}}",
                CardGetInvalidResult = "{\"success\":false,\"statusCode\":200,\"data\":[{\"key\":\"Error\",\"message\":\"barCode can not be null or empty!\"}]}",
                PartnerAddInvalidResult = "{\"success\":false,\"statusCode\":200,\"data\":[{\"key\":\"Error\",\"message\":\"'Email' is not a valid email address.\"}," +
                "{\"key\":\"Error\",\"message\":\"'Phone Number' must not be empty.\"}]}",
                TransactionGetByBarCodeInvalidResult = "{\"success\":false,\"statusCode\":200,\"data\":[{\"key\":\"Error\",\"message\":\"barCode can not be null or empty!\"}]}",
                SuccessEmptyResultForModels = "{\"success\":true,\"statusCode\":200,\"data\":[]}",
                SuccessEmptyResultForModel = "\"{\"success\":true,\"statusCode\":200,\"data\":null}\""
            };
        }

        internal static UrlForTests GetUrlForTests() => new UrlForTests { Url = "http://localhost:52650/api/" };

        internal static ControllerNames GetControllerNames()
        {
            return new ControllerNames
            {
                Bonus = "Bonus",
                Card = "Card",
                Partner = "Partner",
                Transaction = "Transaction"
            };
        }

        internal static ActionNames GetActionNames()
        {
            return new ActionNames
            {
                Add = "Add",
                Assign = "Assign",
                Filter = "Filter",
                Get = "Get",
                GetByBarCode = "GetByBarCode",
                GetByUserId = "GetByUserId",
                Out = "Out"
            };
        }
    }
}
