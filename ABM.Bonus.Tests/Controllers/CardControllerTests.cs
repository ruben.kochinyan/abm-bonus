﻿using ABM.Bonus.API;
using ABM.Bonus.Models.Card;
using ABM.Bonus.Models.Core;
using Newtonsoft.Json;
using System;
using System.Threading.Tasks;
using Xunit;

namespace ABM.Bonus.Tests.Controllers
{
    public sealed class CardControllerTests : ControllerTestsBase
    {
        public CardControllerTests(InternalWebApplicationFactory<Startup> factory) : base(factory)
        {
            ControllerName = Controller.Card;
            BarCode = Guid.NewGuid().ToString().Substring(1, 8);
        }
        
        private string ControllerName { get; }
        private string BarCode { get; }

        [Fact]
        internal async Task Add_SuccessRequestModel_ReturnsSuccessResponseModel()
        {
            var requestModel = ModelsSeed.AddCardRequestModel(BarCode, 12, null);

            using (var httpResponse = await client.PostAsync($"{Url}{ControllerName}/{Action.Add}", Content(requestModel)))
            {
                string expected = await httpResponse.Content.ReadAsStringAsync();
                string actual = configurationService.ControllerTestResultsConfiguration().SuccessResultWithoutModel;

                Assert.Contains(expected, actual);
            }
        }

        [Fact]
        internal async Task Add_InvalidRequestModel_ReturnsErrorResponseModel()
        {
            var requestModel = ModelsSeed.AddCardRequestModel(BarCode, 0, null);

            using (var httpResponse = await client.PostAsync($"{Url}{ControllerName}/{Action.Add}", Content(requestModel)))
            {
                string expected = await httpResponse.Content.ReadAsStringAsync();
                string actual = configurationService.ControllerTestResultsConfiguration().CardAddInvalidResult;

                Assert.Contains(expected, actual);
            }
        }

        [Fact]
        internal async Task Assign_SuccessRequestModel_ReturnsSuccessResponseModel()
        {
            var requestModel = ModelsSeed.AssignRequestModel("1548694512", "testfirstName", "testlastname", "testphonenumber", "testserial");

            using (var httpResponse = await client.PostAsync($"{Url}{ControllerName}/{Action.Assign}", Content(requestModel)))
            {
                string expected = await httpResponse.Content.ReadAsStringAsync();
                string actual = configurationService.ControllerTestResultsConfiguration().SuccessResultWithoutModel;

                Assert.Contains(expected, actual);
            }
        }

        [Fact]
        internal async Task Assign_InvalidRequestModel_ReturnsErrorResponseModel()
        {
            var requestModel = ModelsSeed.AssignRequestModel(string.Empty, string.Empty, "testlastname", "testphonenumber", string.Empty);

            using (var httpResponse = await client.PostAsync($"{Url}{ControllerName}/{Action.Assign}", Content(requestModel)))
            {
                string expexted = await httpResponse.Content.ReadAsStringAsync();
                string actual = configurationService.ControllerTestResultsConfiguration().CardAssignInvalidResult;

                Assert.Contains(expexted, actual);
            }
        }

        [Fact]
        internal async Task Get_SuccessRequestParam_ReturnsSuccessCardResponseModel()
        {
            string barCode = "1548694512";
            var actual = ModelsSeed.GetApiResponseModel(ModelsSeed.GetCardResponseModel());

            using (var httpResponse = await client.GetAsync($"{Url}{ControllerName}/{Action.Get}?barCode={barCode}"))
            {
                string expectedResult = await httpResponse.Content.ReadAsStringAsync();
                var expected = JsonConvert.DeserializeObject<ApiResponseModel<CardResponseModel>>(expectedResult);

                Assert.IsType(expected.GetType(), actual);
            }
        }

        [Fact]
        internal async Task Get_InvalidRequestParam_ReturnsErrorResponseModel()
        {
            using (var httpResponse = await client.GetAsync($"{Url}{ControllerName}/{Action.Get}?barCode={string.Empty}"))
            {
                string expected = await httpResponse.Content.ReadAsStringAsync();
                string actual = configurationService.ControllerTestResultsConfiguration().CardGetInvalidResult;

                Assert.Contains(expected, actual);
            }
        }

        [Fact]
        internal async Task GetByUserId_SuccessRequest_ReturnsSuccessCardResponseModel()
        {
            var actual = ModelsSeed.GetApiResponseModel(ModelsSeed.GetCardResponseModel());

            using (var httpResponse = await client.GetAsync($"{Url}{ControllerName}/{Action.GetByUserId}"))
            {
                string expectedResult = await httpResponse.Content.ReadAsStringAsync();
                var expected = JsonConvert.DeserializeObject<ApiResponseModel<CardResponseModel>>(expectedResult);

                Assert.IsType(expected.GetType(), actual);
            }
        }

        [Fact]
        internal async Task GetByUserId_InvalidRequest_ReturnsEmptyResponseModel()
        {
            using (var httpResponse = await client.GetAsync($"{Url}{ControllerName}/{Action.GetByUserId}"))
            {
                string expected = await httpResponse.Content.ReadAsStringAsync();
                string actual = configurationService.ControllerTestResultsConfiguration().SuccessEmptyResultForModel;

                Assert.Contains(expected, actual);
            }
        }
    }
}
