﻿using ABM.Bonus.API;
using System.Threading.Tasks;
using Xunit;

namespace ABM.Bonus.Tests.Controllers
{
    public sealed class PartnerControllerTests : ControllerTestsBase
    {
        public PartnerControllerTests(InternalWebApplicationFactory<Startup> factory) : base(factory)
        {
            ControllerName = Controller.Partner;
        }

        private string ControllerName { get; }

        [Fact]
        internal async Task Add_SuccessRequestModel_ReturnsSuccessResponseModel()
        {
            var requestModel = ModelsSeed.AddPartnerRequestModel("testName", "test@gmail.com", "testPhoneNumber");

            using (var httpResponse = await client.PostAsync($"{Url}{ControllerName}/{Action.Add}", Content(requestModel)))
            {
                string expected = await httpResponse.Content.ReadAsStringAsync();
                string actual = configurationService.ControllerTestResultsConfiguration().SuccessResultWithoutModel;

                Assert.Contains(expected, actual);
            }
        }

        [Fact]
        internal async Task Add_InvalidRequestModel_ReturnsErrorResponseModel()
        {
            var requestModel = ModelsSeed.AddPartnerRequestModel("testName", "testgmail", string.Empty);

            using (var httpResponse = await client.PostAsync($"{Url}{ControllerName}/{Action.Add}", Content(requestModel)))
            {
                string expected = await httpResponse.Content.ReadAsStringAsync();
                string actual = configurationService.ControllerTestResultsConfiguration().PartnerAddInvalidResult;

                Assert.Contains(expected, actual);
            }
        }
    }
}
