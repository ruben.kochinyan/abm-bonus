﻿using ABM.Bonus.API;
using ABM.Bonus.Entities;
using ABM.Bonus.Models.Core;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xunit;

namespace ABM.Bonus.Tests.Controllers
{
    public sealed class TransactionControllerTests : ControllerTestsBase
    {
        public TransactionControllerTests(InternalWebApplicationFactory<Startup> factory) : base(factory)
        {
            Now = DateTime.UtcNow;
            ControllerName = Controller.Transaction;
        }

        private DateTime Now { get; }
        private string ControllerName { get; }

        [Theory]
        [InlineData(1)]
        internal async Task GetByUserId_SuccessRequestParam_ReturnsSuccessTransactionResponseModels(int userId)
        {
            var actual = ModelsSeed.GetApiResponseModel(ModelsSeed.GetListOfTransactionFacts());

            using (var httpResponse = await client.GetAsync($"{Url}{ControllerName}/{Action.GetByUserId}?userId={userId}"))
            {
                string expectedResult = await httpResponse.Content.ReadAsStringAsync();
                var expected = JsonConvert.DeserializeObject<ApiResponseModel<List<TransactionFact>>>(expectedResult);

                Assert.IsType(expected.GetType(), actual);
            }
        }

        [Theory]
        [InlineData(0)]
        [InlineData(4568784)]
        internal async Task GetByUserId_InvalidRequestParam_ReturnsErrorResponseModel(int userId)  
        {
            using (var httpResponse = await client.GetAsync($"{Url}{ControllerName}/{Action.GetByUserId}?userId={userId}"))
            {
                string expected = await httpResponse.Content.ReadAsStringAsync();
                string actual = configurationService.ControllerTestResultsConfiguration().SuccessEmptyResultForModels;

                Assert.Contains(expected, actual);
            }
        }

        [Fact]
        internal async Task GetByBarCode_SuccessRequestParam_ReturnsSuccessTransactionResponseModels()
        {
            string barCode = "testCode122";

            var actual = ModelsSeed.GetApiResponseModel(ModelsSeed.GetListOfTransactionFacts());

            using (var httpResponse = await client.GetAsync($"{Url}{ControllerName}/{Action.GetByBarCode}?barCode={barCode}"))
            {
                string expectedResult = await httpResponse.Content.ReadAsStringAsync();
                var expected = JsonConvert.DeserializeObject<ApiResponseModel<List<TransactionFact>>>(expectedResult);

                Assert.IsType(expected.GetType(), actual);
            }
        }

        [Fact]
        internal async Task GetByBarCode_InvalidRequestParam_ReturnsErrorResponseModel()
        {
            string barCode = "cpde45";

            using (var httpResponse = await client.GetAsync($"{Url}{ControllerName}/{Action.GetByBarCode}?barCode={barCode}"))
            {
                string expected = await httpResponse.Content.ReadAsStringAsync();
                string actual = configurationService.ControllerTestResultsConfiguration().TransactionGetByBarCodeInvalidResult;

                Assert.Contains(expected, actual);
            }
        }

        [Fact]
        internal async Task Filter_SuccessRequestModel_ReturnsSuccessTransactionResponseModelWhenFromAndToHasValues()
        {
            var requestModel = ModelsSeed.TransactionFilterRequestModel(null, Now.AddDays(-1), Now.AddDays(1), null, null, null);
            var actual = ModelsSeed.GetApiResponseModel(ModelsSeed.GetListOfTransactionFacts());

            using (var httpResponse = await client.PostAsync($"{Url}{ControllerName}/{Action.Filter}", Content(requestModel)))
            {
                string expectedResult = await httpResponse.Content.ReadAsStringAsync();
                var expected = JsonConvert.DeserializeObject<ApiResponseModel<List<TransactionFact>>>(expectedResult);

                Assert.IsType(expected.GetType(), actual);
            }
        }

        [Fact]
        internal async Task Filter_SuccessRequestModel_ReturnsSuccessTransactionResponseModelWhenFromHasValue()
        {
            var requestModel = ModelsSeed.TransactionFilterRequestModel(null, Now.AddDays(-1), null, null, null, null);
            var actual = ModelsSeed.GetApiResponseModel(ModelsSeed.GetListOfTransactionFacts());

            using (var httpResponse = await client.PostAsync($"{Url}{ControllerName}/{Action.Filter}", Content(requestModel)))
            {
                string expectedResult = await httpResponse.Content.ReadAsStringAsync();
                var expected = JsonConvert.DeserializeObject<ApiResponseModel<List<TransactionFact>>>(expectedResult);

                Assert.IsType(expected.GetType(), actual);
            }
        }

        [Fact]
        internal async Task Filter_SuccessRequestModel_ReturnsSuccessTransactionResponseModelWhenToHasValue()
        {
            var requestModel = ModelsSeed.TransactionFilterRequestModel(null, null, Now.AddDays(1), null, null, null);
            var actual = ModelsSeed.GetApiResponseModel(ModelsSeed.GetListOfTransactionFacts());

            using (var httpResponse = await client.PostAsync($"{Url}{ControllerName}/{Action.Filter}", Content(requestModel)))
            {
                string expectedResult = await httpResponse.Content.ReadAsStringAsync();
                var expected = JsonConvert.DeserializeObject<ApiResponseModel<List<TransactionFact>>>(expectedResult);

                Assert.IsType(expected.GetType(), actual);
            }
        }

        [Fact]
        internal async Task Filter_SuccessRequestModel_ReturnsSuccessTransactionResponseModelWhenConcreteDayHasValue()
        {
            var requestModel = ModelsSeed.TransactionFilterRequestModel(null, null, null, Now, null, null);
            var actual = ModelsSeed.GetApiResponseModel(ModelsSeed.GetListOfTransactionFacts());

            using (var httpResponse = await client.PostAsync($"{Url}{ControllerName}/{Action.Filter}", Content(requestModel)))
            {
                string expectedResult = await httpResponse.Content.ReadAsStringAsync();
                var expected = JsonConvert.DeserializeObject<ApiResponseModel<List<TransactionFact>>>(expectedResult);

                Assert.IsType(expected.GetType(), actual);
            }
        }

        [Fact]
        internal async Task Filter_SuccessRequestModel_ReturnsSuccessTransactionResponseModelWhenBonusFromAndBonusToHasValues()
        {
            var requestModel = ModelsSeed.TransactionFilterRequestModel(null, null, null, null, 10, 50);
            var actual = ModelsSeed.GetApiResponseModel(ModelsSeed.GetListOfTransactionFacts());

            using (var httpResponse = await client.PostAsync($"{Url}{ControllerName}/{Action.Filter}", Content(requestModel)))
            {
                string expectedResult = await httpResponse.Content.ReadAsStringAsync();
                var expected = JsonConvert.DeserializeObject<ApiResponseModel<List<TransactionFact>>>(expectedResult);

                Assert.IsType(expected.GetType(), actual);
            }
        }

        [Fact]
        internal async Task Filter_SuccessRequestModel_ReturnsSuccessTransactionResponseModelWhenBonusFromHasValue()
        {
            var requestModel = ModelsSeed.TransactionFilterRequestModel(null, null, null, null, 30, null);
            var actual = ModelsSeed.GetApiResponseModel(ModelsSeed.GetListOfTransactionFacts());

            using (var httpResponse = await client.PostAsync($"{Url}{ControllerName}/{Action.Filter}", Content(requestModel)))
            {
                string expectedResult = await httpResponse.Content.ReadAsStringAsync();
                var expected = JsonConvert.DeserializeObject<ApiResponseModel<List<TransactionFact>>>(expectedResult);

                Assert.IsType(expected.GetType(), actual);
            }
        }

        [Fact]
        internal async Task Filter_SuccessRequestModel_ReturnsSuccessTransactionResponseModelWhenBonusToHasValue()
        {
            var requestModel = ModelsSeed.TransactionFilterRequestModel(null, null, null, null, null, 120);
            var actual = ModelsSeed.GetApiResponseModel(ModelsSeed.GetListOfTransactionFacts());

            using (var httpResponse = await client.PostAsync($"{Url}{ControllerName}/{Action.Filter}", Content(requestModel)))
            {
                string expectedResult = await httpResponse.Content.ReadAsStringAsync();
                var expected = JsonConvert.DeserializeObject<ApiResponseModel<List<TransactionFact>>>(expectedResult);

                Assert.IsType(expected.GetType(), actual);
            }
        }

        [Fact]
        internal async Task Filter_SuccessRequestModel_ReturnsSuccessTransactionResponseModelWhenUserIdHasValue()
        {
            var requestModel = ModelsSeed.TransactionFilterRequestModel(1, null, null, null, null, null);
            var actual = ModelsSeed.GetApiResponseModel(ModelsSeed.GetListOfTransactionFacts());

            using (var httpResponse = await client.PostAsync($"{Url}{ControllerName}/{Action.Filter}", Content(requestModel)))
            {
                string expectedResult = await httpResponse.Content.ReadAsStringAsync();
                var expected = JsonConvert.DeserializeObject<ApiResponseModel<List<TransactionFact>>>(expectedResult);

                Assert.IsType(expected.GetType(), actual);
            }
        }

        [Fact]
        internal async Task Filter_SuccessRequestModel_ReturnsSuccessTransactionResponseModelWhenUserIdAndFromAndToHasValues()
        {
            var requestModel = ModelsSeed.TransactionFilterRequestModel(1, Now.AddDays(-1), Now.AddDays(1), null, null, null);
            var actual = ModelsSeed.GetApiResponseModel(ModelsSeed.GetListOfTransactionFacts());

            using (var httpResponse = await client.PostAsync($"{Url}{ControllerName}/{Action.Filter}", Content(requestModel)))
            {
                string expectedResult = await httpResponse.Content.ReadAsStringAsync();
                var expected = JsonConvert.DeserializeObject<ApiResponseModel<List<TransactionFact>>>(expectedResult);

                Assert.IsType(expected.GetType(), actual);
            }
        }

        [Fact]
        internal async Task Filter_SuccessRequestModel_ReturnsSuccessTransactionResponseModelWhenUserIdAndFromHasValue()
        {
            var requestModel = ModelsSeed.TransactionFilterRequestModel(1, Now.AddDays(-1), null, null, null, null);
            var actual = ModelsSeed.GetApiResponseModel(ModelsSeed.GetListOfTransactionFacts());

            using (var httpResponse = await client.PostAsync($"{Url}{ControllerName}/{Action.Filter}", Content(requestModel)))
            {
                string expectedResult = await httpResponse.Content.ReadAsStringAsync();
                var expected = JsonConvert.DeserializeObject<ApiResponseModel<List<TransactionFact>>>(expectedResult);

                Assert.IsType(expected.GetType(), actual);
            }
        }

        [Fact]
        internal async Task Filter_SuccessRequestModel_ReturnsSuccessTransactionResponseModelWhenUserIdAndToHasValue()
        {
            var requestModel = ModelsSeed.TransactionFilterRequestModel(1, null, Now.AddDays(1), null, null, null);
            var actual = ModelsSeed.GetApiResponseModel(ModelsSeed.GetListOfTransactionFacts());

            using (var httpResponse = await client.PostAsync($"{Url}{ControllerName}/{Action.Filter}", Content(requestModel)))
            {
                string expectedResult = await httpResponse.Content.ReadAsStringAsync();
                var expected = JsonConvert.DeserializeObject<ApiResponseModel<List<TransactionFact>>>(expectedResult);

                Assert.IsType(expected.GetType(), actual);
            }
        }

        [Fact]
        internal async Task Filter_SuccessRequestModel_ReturnsSuccessTransactionResponseModelWhenUserIdAndConcreteDayHasValue()
        {
            var requestModel = ModelsSeed.TransactionFilterRequestModel(1, null, null, Now, null, null);
            var actual = ModelsSeed.GetApiResponseModel(ModelsSeed.GetListOfTransactionFacts());

            using (var httpResponse = await client.PostAsync($"{Url}{ControllerName}/{Action.Filter}", Content(requestModel)))
            {
                string expectedResult = await httpResponse.Content.ReadAsStringAsync();
                var expected = JsonConvert.DeserializeObject<ApiResponseModel<List<TransactionFact>>>(expectedResult);

                Assert.IsType(expected.GetType(), actual);
            }
        }

        [Fact]
        internal async Task Filter_SuccessRequestModel_ReturnsSuccessTransactionResponseModelWhenUserIdAndBonusFromAndBonusToHasValues()
        {
            var requestModel = ModelsSeed.TransactionFilterRequestModel(1, null, null, null, 10, 50);
            var actual = ModelsSeed.GetApiResponseModel(ModelsSeed.GetListOfTransactionFacts());

            using (var httpResponse = await client.PostAsync($"{Url}{ControllerName}/{Action.Filter}", Content(requestModel)))
            {
                string expectedResult = await httpResponse.Content.ReadAsStringAsync();
                var expected = JsonConvert.DeserializeObject<ApiResponseModel<List<TransactionFact>>>(expectedResult);

                Assert.IsType(expected.GetType(), actual);
            }
        }

        [Fact]
        internal async Task Filter_SuccessRequestModel_ReturnsSuccessTransactionResponseModelWhenUserIdAndBonusFromHasValue()
        {
            var requestModel = ModelsSeed.TransactionFilterRequestModel(1, null, null, null, 30, null);
            var actual = ModelsSeed.GetApiResponseModel(ModelsSeed.GetListOfTransactionFacts());

            using (var httpResponse = await client.PostAsync($"{Url}{ControllerName}/{Action.Filter}", Content(requestModel)))
            {
                string expectedResult = await httpResponse.Content.ReadAsStringAsync();
                var expected = JsonConvert.DeserializeObject<ApiResponseModel<List<TransactionFact>>>(expectedResult);

                Assert.IsType(expected.GetType(), actual);
            }
        }

        [Fact]
        internal async Task Filter_SuccessRequestModel_ReturnsSuccessTransactionResponseModelWhenUserIdAndBonusToHasValue()
        {
            var requestModel = ModelsSeed.TransactionFilterRequestModel(1, null, null, null, null, 120);
            var actual = ModelsSeed.GetApiResponseModel(ModelsSeed.GetListOfTransactionFacts());

            using (var httpResponse = await client.PostAsync($"{Url}{ControllerName}/{Action.Filter}", Content(requestModel)))
            {
                string expectedResult = await httpResponse.Content.ReadAsStringAsync();
                var expected = JsonConvert.DeserializeObject<ApiResponseModel<List<TransactionFact>>>(expectedResult);

                Assert.IsType(expected.GetType(), actual);
            }
        }
    }
}
