﻿using ABM.Bonus.API;
using ABM.Bonus.Models.Options;
using ABM.Bonus.Services.Interfaces;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using System.Net.Http;
using System.Text;
using Xunit;

namespace ABM.Bonus.Tests.Controllers
{
    public abstract class ControllerTestsBase : IClassFixture<InternalWebApplicationFactory<Startup>>
    {
        protected readonly HttpClient client;
        protected readonly IConfigurationService configurationService;
        protected readonly ICardService cardService;

        protected ControllerTestsBase(InternalWebApplicationFactory<Startup> factory)
        {
            client = factory.CreateClient();
            configurationService = factory.Services.CreateScope().ServiceProvider.GetService<IConfigurationService>();
            Url = configurationService.UrlForTestsConfiguuration().Url;
            Controller = configurationService.ControllerNamesConfiguration();
            Action = configurationService.ActionNamesConfiguration();
            cardService = factory.Services.CreateScope().ServiceProvider.GetRequiredService<ICardService>();
        }

        protected string Url { get; }
        protected StringContent Content(object requestModel) => new StringContent(JsonConvert.SerializeObject(requestModel), Encoding.UTF8, "application/json");
        protected ControllerNames Controller { get; }
        protected ActionNames Action { get; set; }
    }
}
