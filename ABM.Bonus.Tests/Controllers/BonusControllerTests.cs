﻿using ABM.Bonus.API;
using System.Threading.Tasks;
using Xunit;

namespace ABM.Bonus.Tests.Controllers
{
    public sealed class BonusControllerTests : ControllerTestsBase
    {
        public BonusControllerTests(InternalWebApplicationFactory<Startup> factory) : base(factory)
        {
            ControllerName = Controller.Bonus;
        }

        private string ControllerName { get; }

        [Fact]
        internal async Task Add_SuccessRequestModel_ReturnsSuccessResponseModel()
        {
            var requestModel = ModelsSeed.AddBonusRequestModel("1548694512", 7);

            using (var httpResponse = await client.PostAsync($"{Url}{ControllerName}/{Action.Add}", Content(requestModel)))
            {
                string expected = await httpResponse.Content.ReadAsStringAsync();
                string actual = configurationService.ControllerTestResultsConfiguration().SuccessResultWithoutModel;

                Assert.Contains(expected, actual);
            }
        }

        [Fact]
        internal async Task Add_InvalidRequestModel_RturnsErrorResponseModel()
        {
            var requestModel = ModelsSeed.AddBonusRequestModel(string.Empty, 0);

            using (var httpResponse = await client.PostAsync($"{Url}{ControllerName}/{Action.Add}", Content(requestModel)))
            {
                string expected = await httpResponse.Content.ReadAsStringAsync();
                string actual = configurationService.ControllerTestResultsConfiguration().BonusAddInvalidResult;

                Assert.Contains(expected, actual);
            }
        }

        [Fact]
        internal async Task Out_SuccessRequestModel_ReturnsSuccessResponseModel()
        {
            var requestModel = ModelsSeed.BonusOutRequestModel("1548694512", 11);

            using (var httpResponse = await client.PostAsync($"{Url}{ControllerName}/{Action.Out}", Content(requestModel)))
            {
                string expected = await httpResponse.Content.ReadAsStringAsync();
                string actual = configurationService.ControllerTestResultsConfiguration().SuccessResultWithoutModel;

                Assert.Contains(expected, actual);
            }
        }

        [Fact]
        internal async Task Out_InvalidRequestModel_ReturnsErrorResponseMode()
        {
            var requestModel = ModelsSeed.BonusOutRequestModel(string.Empty, 11);

            using (var httpResponse = await client.PostAsync($"{Url}{ControllerName}/{Action.Out}", Content(requestModel)))
            {
                string expected = await httpResponse.Content.ReadAsStringAsync();
                string actual = configurationService.ControllerTestResultsConfiguration().BonusOutInvalidResult;

                Assert.Contains(expected, actual);
            }
        }

        [Fact]
        internal async Task Out_InvalidBonusQuantity_ReturnsErrorResponseModel()
        {
            var requestModel = ModelsSeed.BonusOutRequestModel("1548694512", int.MaxValue);

            using (var httpResponse = await client.PostAsync($"{Url}{ControllerName}/{Action.Out}", Content(requestModel)))
            {
                string expected = await httpResponse.Content.ReadAsStringAsync();
                string actual = configurationService.ControllerTestResultsConfiguration().BonusOutNotEnoughBalanceResult;

                Assert.Contains(expected, actual);
            }
        }
    }
}
