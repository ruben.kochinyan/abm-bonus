﻿using ABM.Bonus.API;
using ABM.Bonus.Services.Interfaces;
using Xunit;

namespace ABM.Bonus.Tests.Services
{
    public sealed class ConfigurationServiceTests : ServiceTestsBase<IConfigurationService>
    {
        public ConfigurationServiceTests(InternalWebApplicationFactory<Startup> factory) : base(factory)
        {
        }

        [Fact]
        internal void ControllerTestResultsConfiguration_CheckBonusAddInvalidResult_ReturnsControllerTestResults()
        {
            var expected = service.ControllerTestResultsConfiguration();
            var actual = ModelsSeed.GetControllerTestResults();

            Assert.Contains(expected.BonusAddInvalidResult, actual.BonusAddInvalidResult);
        }

        [Fact]
        internal void ControllerTestResultsConfiguration_CheckBonusOutInvalidResult_ReturnsControllerTestResults()
        {
            var expected = service.ControllerTestResultsConfiguration();
            var actual = ModelsSeed.GetControllerTestResults();

            Assert.Contains(expected.BonusOutInvalidResult, actual.BonusOutInvalidResult);
        }

        [Fact]
        internal void ControllerTestResultsConfiguration_CheckBonusOutNotEnoughBalanceResult_ReturnsControllerTestResults()
        {
            var expected = service.ControllerTestResultsConfiguration();
            var actual = ModelsSeed.GetControllerTestResults();

            Assert.Contains(expected.BonusOutNotEnoughBalanceResult, actual.BonusOutNotEnoughBalanceResult);
        }

        [Fact]
        internal void ControllerTestResultsConfiguration_CheckCardAddInvalidResult_ReturnsControllerTestResults()
        {
            var expected = service.ControllerTestResultsConfiguration();
            var actual = ModelsSeed.GetControllerTestResults();

            Assert.Contains(expected.CardAddInvalidResult, actual.CardAddInvalidResult);
        }

        [Fact]
        internal void ControllerTestResultsConfiguration_CardAssignInvalidResult_ReturnsControllerTestResults()
        {
            var expected = service.ControllerTestResultsConfiguration();
            var actual = ModelsSeed.GetControllerTestResults();

            Assert.Contains(expected.CardAssignInvalidResult, actual.CardAssignInvalidResult);
        }

        [Fact]
        internal void ControllerTestResultsConfiguration_CheckCardGetInvalidResult_ReturnsControllerTestResults()
        {
            var expected = service.ControllerTestResultsConfiguration();
            var actual = ModelsSeed.GetControllerTestResults();

            Assert.Contains(expected.CardGetInvalidResult, actual.CardGetInvalidResult);
        }

        [Fact]
        internal void ControllerTestResultsConfiguration_CheckCardGetSuccessResult_ReturnsControllerTestResults()
        {
            var expected = service.ControllerTestResultsConfiguration();
            var actual = ModelsSeed.GetControllerTestResults();

            Assert.Contains(expected.CardGetSuccessResult, actual.CardGetSuccessResult);
        }

        [Fact]
        internal void ControllerTestResultsConfiguration_CheckPartnerAddInvalidResult_ReturnsControllerTestResults()
        {
            var expected = service.ControllerTestResultsConfiguration();
            var actual = ModelsSeed.GetControllerTestResults();

            Assert.Contains(expected.PartnerAddInvalidResult, actual.PartnerAddInvalidResult);
        }

        [Fact]
        internal void ControllerTestResultsConfiguration_CheckSuccessEmptyResultForModel_ReturnsControllerTestResults()
        {
            var expected = service.ControllerTestResultsConfiguration();
            var actual = ModelsSeed.GetControllerTestResults();

            Assert.Contains(expected.SuccessEmptyResultForModel, actual.SuccessEmptyResultForModel);
        }

        [Fact]
        internal void ControllerTestResultsConfiguration_CheckSuccessEmptyResultForModels_ReturnsControllerTestResults()
        {
            var expected = service.ControllerTestResultsConfiguration();
            var actual = ModelsSeed.GetControllerTestResults();

            Assert.Contains(expected.SuccessEmptyResultForModels, actual.SuccessEmptyResultForModels);
        }

        [Fact]
        internal void ControllerTestResultsConfiguration_CheckSuccessResultWithoutModel_ReturnsControllerTestResults()
        {
            var expected = service.ControllerTestResultsConfiguration();
            var actual = ModelsSeed.GetControllerTestResults();

            Assert.Contains(expected.SuccessResultWithoutModel, actual.SuccessResultWithoutModel);
        }

        [Fact]
        internal void ControllerTestResultsConfiguration_CheckTransactionGetByBarCodeInvalidResult_ReturnsControllerTestResults()
        {
            var expected = service.ControllerTestResultsConfiguration();
            var actual = ModelsSeed.GetControllerTestResults();

            Assert.Contains(expected.TransactionGetByBarCodeInvalidResult, actual.TransactionGetByBarCodeInvalidResult);
        }

        [Fact]
        internal void UrlForTestsConfiguuration_CheckUrl_ReturnsUrlForTests()
        {
            var expected = service.UrlForTestsConfiguuration();
            var actual = ModelsSeed.GetUrlForTests();

            Assert.Contains(expected.Url, actual.Url);
        }

        [Fact]
        internal void ControllerNamesConfiguration_CheckBonus_ReturnsControllerNames()
        {
            var expected = service.ControllerNamesConfiguration();
            var actual = ModelsSeed.GetControllerNames();

            Assert.Contains(expected.Bonus, actual.Bonus);
        }
        [Fact]
        internal void ControllerNamesConfiguration_CheckCard_ReturnsControllerNames()
        {
            var expected = service.ControllerNamesConfiguration();
            var actual = ModelsSeed.GetControllerNames();

            Assert.Contains(expected.Card, actual.Card);
        }

        [Fact]
        internal void ControllerNamesConfiguration_CheckPartner_ReturnsControllerNames()
        {
            var expected = service.ControllerNamesConfiguration();
            var actual = ModelsSeed.GetControllerNames();

            Assert.Contains(expected.Partner, actual.Partner);
        }

        [Fact]
        internal void ControllerNamesConfiguration_CheckTransaction_ReturnsControllerNames()
        {
            var expected = service.ControllerNamesConfiguration();
            var actual = ModelsSeed.GetControllerNames();

            Assert.Contains(expected.Transaction, actual.Transaction);
        }

        [Fact]
        internal void ActionNamesConfiguration_CheckAdd_ReturnsActionNames()
        {
            var expected = service.ActionNamesConfiguration();
            var actual = ModelsSeed.GetActionNames();

            Assert.Contains(expected.Add, actual.Add);
        }

        [Fact]
        internal void ActionNamesConfiguration_CheckAssign_ReturnsActionNames()
        {
            var expected = service.ActionNamesConfiguration();
            var actual = ModelsSeed.GetActionNames();

            Assert.Contains(expected.Assign, actual.Assign);
        }

        [Fact]
        internal void ActionNamesConfiguration_CheckFilter_ReturnsActionNames()
        {
            var expected = service.ActionNamesConfiguration();
            var actual = ModelsSeed.GetActionNames();

            Assert.Contains(expected.Filter, actual.Filter);
        }

        [Fact]
        internal void ActionNamesConfiguration_CheckGet_ReturnsActionNames()
        {
            var expected = service.ActionNamesConfiguration();
            var actual = ModelsSeed.GetActionNames();

            Assert.Contains(expected.Get, actual.Get);
        }

        [Fact]
        internal void ActionNamesConfiguration_CheckGetByBarCode_ReturnsActionNames()
        {
            var expected = service.ActionNamesConfiguration();
            var actual = ModelsSeed.GetActionNames();

            Assert.Contains(expected.GetByBarCode, actual.GetByBarCode);
        }
        [Fact]
        internal void ActionNamesConfiguration_CheckGetByUserId_ReturnsActionNames()
        {
            var expected = service.ActionNamesConfiguration();
            var actual = ModelsSeed.GetActionNames();

            Assert.Contains(expected.GetByUserId, actual.GetByUserId);
        }
        [Fact]
        internal void ActionNamesConfiguration_CheckOut_ReturnsActionNames()
        {
            var expected = service.ActionNamesConfiguration();
            var actual = ModelsSeed.GetActionNames();

            Assert.Contains(expected.Out, actual.Out);
        }
    }
}
