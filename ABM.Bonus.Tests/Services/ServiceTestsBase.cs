﻿using ABM.Bonus.API;
using Microsoft.Extensions.DependencyInjection;
using Xunit;

namespace ABM.Bonus.Tests.Services
{
    public abstract class ServiceTestsBase<TService> : IClassFixture<InternalWebApplicationFactory<Startup>>
    {
        protected readonly TService service;

        protected ServiceTestsBase(InternalWebApplicationFactory<Startup> factory)
        {
            service = factory.Services.CreateScope().ServiceProvider.GetRequiredService<TService>();
        }
    }
}
