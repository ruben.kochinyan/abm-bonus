﻿using ABM.Bonus.API;
using ABM.Bonus.Entities;
using ABM.Bonus.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace ABM.Bonus.Tests.Services
{
    public sealed class TransactionServiceTests : ServiceTestsBase<ITransactionService>
    {
        public TransactionServiceTests(InternalWebApplicationFactory<Startup> factory) : base(factory)
        {
            Now = DateTime.UtcNow;
            UserId = 11;
            BarCode = "1548694512";
            PartnerId = 1;
        }

        private DateTime Now { get; }
        private int UserId { get; }
        private string BarCode { get; }
        private int PartnerId { get; }

        [Fact]
        internal async Task GetByUserIdAsync_SuccessParam_ReturnsTransactionFacts_CheckByBareCode()
        {
            var serviceResult = await service.GetByUserIdAsync(UserId);

            Assert.True(serviceResult.First().CardBarCode == BarCode);
        }

        [Fact]
        internal async Task GetByUserIdAsync_SuccessParam_ReturnsTransactionFacts()
        {
            var facts = new List<TransactionFact>();
            IEnumerable<TransactionFact> serviceResult = await service.GetByUserIdAsync(UserId);

            Assert.IsType(serviceResult.GetType(), facts);
        }

        [Fact]
        internal async Task GetByUserIdAsync_InvalidParam_ReturnsEmpty()
        {
            IEnumerable<TransactionFact> serviceResult = await service.GetByUserIdAsync(int.MaxValue);

            Assert.True(serviceResult.Count() == 0);
        }

        [Fact]
        internal async Task GetByBarCodeAsync_SuccessParam_ReturnsTransactionFacts_CheckByUserId()
        {
            IEnumerable<TransactionFact> serviceResult = await service.GetByBarCodeAsync(BarCode);

            Assert.True(serviceResult.First().CardBarCode == BarCode);
        }

        [Fact]
        internal async Task GetByBarCodeAsync_SuccessParam_ReturnsTransactionFacts()
        {
            var facts = new List<TransactionFact>();
            IEnumerable<TransactionFact> serviceResult = await service.GetByBarCodeAsync(BarCode);

            Assert.IsType(serviceResult.GetType(), facts);
        }

        [Fact]
        internal async Task GetByBarCodeAsync_InvalidParam_ReturnsEmpty()
        {
            IEnumerable<TransactionFact> serviceResult = await service.GetByBarCodeAsync(string.Empty);

            Assert.True(serviceResult.Count() == 0);
        }

        [Fact]
        internal async Task Filter_SuccessModelWhenFromAndToHasValues_ReturnsTransactionFacts_CheckByPartnerId()
        {
            var model = ModelsSeed.TransactionFilterRequestModel(null, Now.AddDays(-1), Now.AddDays(1), null, null, null);
            IEnumerable<TransactionFact> serviceResult = await service.Filter(model);

            Assert.True(serviceResult.First().PartnerId == PartnerId);
        }

        [Fact]
        internal async Task Filter_SuccessModelWhenFromHasValue_ReturnsTransactionFacts_CheckByPartnerId()
        {
            var model = ModelsSeed.TransactionFilterRequestModel(null, Now.AddDays(-1), null, null, null, null);
            IEnumerable<TransactionFact> serviceResult = await service.Filter(model);

            Assert.True(serviceResult.First().PartnerId == PartnerId);
        }

        [Fact]
        internal async Task Filter_SuccessModelWhenToHasValue_ReturnsTransactionFacts_CheckByPartnerId()
        {
            var model = ModelsSeed.TransactionFilterRequestModel(null, null, Now.AddDays(1), null, null, null);
            IEnumerable<TransactionFact> serviceResult = await service.Filter(model);

            Assert.True(serviceResult.First().PartnerId == PartnerId);
        }

        [Fact]
        internal async Task Filter_SuccessModelWhenConcreteDayHasValue_ReturnsTransactionFacts_CheckByPartnerId()
        {
            var model = ModelsSeed.TransactionFilterRequestModel(null, null, null, Now, null, null);
            IEnumerable<TransactionFact> serviceResult = await service.Filter(model);

            Assert.True(serviceResult.First().PartnerId == PartnerId);
        }

        [Fact]
        internal async Task Filter_SuccessModelWhenBonusFromAndBonusToHasValues_ReturnsTransactionFacts_CheckByPartnerId()
        {
            var model = ModelsSeed.TransactionFilterRequestModel(null, null, null, null, 10, 50);
            IEnumerable<TransactionFact> serviceResult = await service.Filter(model);

            Assert.True(serviceResult.First().PartnerId == PartnerId);
        }

        [Fact]
        internal async Task Filter_SuccessModelWhenBonusFromHasValue_ReturnsTransactionFacts_CheckByPartnerId()
        {
            var model = ModelsSeed.TransactionFilterRequestModel(null, null, null, null, 30, null);
            IEnumerable<TransactionFact> serviceResult = await service.Filter(model);

            Assert.True(serviceResult.First().PartnerId == PartnerId);
        }

        [Fact]
        internal async Task Filter_SuccessModelWhenBonusToHasValue_ReturnsTransactionFacts_CheckByPartnerId()
        {
            var model = ModelsSeed.TransactionFilterRequestModel(null, null, null, null, null, 120);
            IEnumerable<TransactionFact> serviceResult = await service.Filter(model);

            Assert.True(serviceResult.First().PartnerId == PartnerId);
        }

        [Fact]
        internal async Task Filter_SuccessModelWhenUserIdHasValue_ReturnsTransactionFacts_CheckByPartnerId()
        {
            var model = ModelsSeed.TransactionFilterRequestModel(UserId, null, null, null, null, null);
            IEnumerable<TransactionFact> serviceResult = await service.Filter(model);

            Assert.True(serviceResult.First().PartnerId == PartnerId);
        }

        [Fact]
        internal async Task Filter_SuccessModelWhenUserIdAndFromAndToHasValues_ReturnsTransactionFacts_CheckByPartnerId()
        {
            var model = ModelsSeed.TransactionFilterRequestModel(UserId, Now.AddDays(-1), Now.AddDays(1), null, null, null);
            IEnumerable<TransactionFact> serviceResult = await service.Filter(model);

            Assert.True(serviceResult.First().PartnerId == PartnerId);
        }

        [Fact]
        internal async Task Filter_SuccessModelWhenUserIdAndFromHasValue_ReturnsTransactionFacts_CheckByPartnerId()
        {
            var model = ModelsSeed.TransactionFilterRequestModel(UserId, Now.AddDays(-1), null, null, null, null);
            IEnumerable<TransactionFact> serviceResult = await service.Filter(model);

            Assert.True(serviceResult.First().PartnerId == PartnerId);
        }

        [Fact]
        internal async Task Filter_SuccessModelWhenUserIdAndToHasValue_ReturnsTransactionFacts_CheckByPartnerId()
        {
            var model = ModelsSeed.TransactionFilterRequestModel(UserId, null, Now.AddDays(1), null, null, null);
            IEnumerable<TransactionFact> serviceResult = await service.Filter(model);

            Assert.True(serviceResult.First().PartnerId == PartnerId);
        }

        [Fact]
        internal async Task Filter_SuccessModelWhenUserIdAndConcreteDayHasValue_ReturnsTransactionFacts_CheckByPartnerId()
        {
            var model = ModelsSeed.TransactionFilterRequestModel(UserId, null, null, Now, null, null);
            IEnumerable<TransactionFact> serviceResult = await service.Filter(model);

            Assert.True(serviceResult.First().PartnerId == PartnerId);
        }

        [Fact]
        internal async Task Filter_SuccessModelWhenUserIdAndBonusFromAndBonusToHasValues_ReturnsTransactionFacts_CheckByPartnerId()
        {
            var model = ModelsSeed.TransactionFilterRequestModel(UserId, null, null, null, 10, 50);
            IEnumerable<TransactionFact> serviceResult = await service.Filter(model);

            Assert.True(serviceResult.First().PartnerId == PartnerId);
        }

        [Fact]
        internal async Task Filter_SuccessModelWhenUserIdAndBonusFromHasValue_ReturnsTransactionFcats_CheckByPartnerId()
        {
            var model = ModelsSeed.TransactionFilterRequestModel(UserId, null, null, null, 30, null);
            IEnumerable<TransactionFact> serviceResult = await service.Filter(model);

            Assert.True(serviceResult.First().PartnerId == PartnerId);
        }

        [Fact]
        internal async Task Filter_SuccessModelWhenUserIdAndBonusToHasValue_ReturnsTransactionFacts_CheckByPartnerId()
        {
            var model = ModelsSeed.TransactionFilterRequestModel(UserId, null, null, null, null, 120);
            IEnumerable<TransactionFact> serviceResult = await service.Filter(model);

            Assert.True(serviceResult.First().PartnerId == PartnerId);
        }
    }
}
