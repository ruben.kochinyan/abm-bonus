﻿using ABM.Bonus.API;
using ABM.Bonus.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace ABM.Bonus.Tests.Services
{
    public sealed class UserServiceTests : ServiceTestsBase<IUserService>
    {
        public UserServiceTests(InternalWebApplicationFactory<Startup> factory) : base(factory)
        {
        }
    }
}
