﻿using ABM.Bonus.API;
using ABM.Bonus.Entities;
using ABM.Bonus.Services.Interfaces;
using System.Threading.Tasks;
using Xunit;

namespace ABM.Bonus.Tests.Services
{
    public sealed class CardServiceTests : ServiceTestsBase<ICardService>
    {
        public CardServiceTests(InternalWebApplicationFactory<Startup> factory) : base(factory)
        {
            BarCode = "1548694512";
            UserId = 12;
        }

        private string BarCode { get; }
        private int UserId { get; }

        [Fact]
        internal async Task GetByUserIdAsync_SuccessParam_ReturnsCard_CheckByBareCode()
        {
            Card card = ModelsSeed.GetCard();
            card.BarCode = BarCode;

            var serviceResult = await service.GetByUserIdAsync(UserId);

            Assert.True(card.BarCode == serviceResult.BarCode);
        }

        [Fact]
        internal async Task GetByUserIdAsync_SuccessParam_ReturnsCard()
        {
            var serviceReuslt = await service.GetByUserIdAsync(UserId);

            Assert.IsType(serviceReuslt.GetType(), ModelsSeed.GetCard());
        }

        [Fact]
        internal async Task GetByUserIdAsync_InvalidParam_ReturnsNull()
        {
            var serviceResult = await service.GetByUserIdAsync(int.MaxValue);

            Assert.True(serviceResult == null);
        }

        [Fact]
        internal async Task GetByBarCodeAsync_SuccessParam_ReturnsCard_CheckByUserId()
        {
            var serviceResult = await service.GetByBarCodeAsync(BarCode);

            Assert.True(serviceResult.UserId == UserId);
        }

        [Fact]
        internal async Task GetByBarCodeAsync_SuccessParam_ReturnsCard()
        {
            var serviceResult = await service.GetByBarCodeAsync(BarCode);

            Assert.IsType(serviceResult.GetType(), ModelsSeed.GetCard());
        }

        [Fact]
        internal async Task GetByBarCodeAsync_InvalidParam_ReturnsNull()
        {
            var serviceResult = await service.GetByBarCodeAsync(string.Empty);

            Assert.True(serviceResult == null);
        }
    }
}
