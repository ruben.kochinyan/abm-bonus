﻿using ABM.Bonus.DAL.Configurations;
using ABM.Bonus.Entities;
using Microsoft.EntityFrameworkCore;

namespace ABM.Bonus.DAL
{
    public class BonusDbContext : DbContext
    {
        public BonusDbContext(DbContextOptions<BonusDbContext> options) : base(options)
        {
        }

        public DbSet<Partner> Partners { get; set; }
        public DbSet<Card> Cards { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<TransactionFact> TransactionFacts { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new PartnerConfiguration());
            modelBuilder.ApplyConfiguration(new CardConfiguration());
            modelBuilder.ApplyConfiguration(new UserConfiguration());
            modelBuilder.ApplyConfiguration(new TransactionFactConfiguration());
        }
    }
}
