﻿using ABM.Bonus.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using System.Linq.Expressions;

namespace ABM.Bonus.DAL
{
    public class Repository<TEntity> : IRepository<TEntity>
        where TEntity : EntityBase
    {
        private readonly BonusDbContext dbContext;
        private readonly DbSet<TEntity> dbSet;

        public Repository(BonusDbContext dbContext)
        {
            this.dbContext = dbContext;
            this.dbSet = dbContext.Set<TEntity>();
        }

        public void Add(TEntity entity)
        {
            DateTime now = DateTime.UtcNow;

            entity.CreatedDate = now;
            entity.UpdatedDate = now;

            dbSet.Add(entity);
        }

        public async Task AddAsync(TEntity entity)
        {
            DateTime now = DateTime.UtcNow;

            entity.CreatedDate = now;
            entity.UpdatedDate = now;

            await dbSet.AddAsync(entity);
        }

        public void AddRange(IEnumerable<TEntity> entities)
        {
            DateTime now = DateTime.UtcNow;

            foreach (TEntity entity in entities)
            {
                entity.CreatedDate = now;
                entity.UpdatedDate = now;
            }

            dbSet.AddRange(entities);
        }

        public async Task AddRangeAsync(IEnumerable<TEntity> entities)
        {
            DateTime now = DateTime.UtcNow;

            foreach (TEntity entity in entities)
            {
                entity.CreatedDate = now;
                entity.UpdatedDate = now;
            }

            await dbSet.AddRangeAsync(entities);
        }

        public void Update(TEntity entity)
        {
            entity.UpdatedDate = DateTime.UtcNow;
            dbSet.Update(entity);
        }

        public void UpdateRange(IEnumerable<TEntity> entities)
        {
            DateTime now = DateTime.UtcNow;

            foreach (TEntity entity in entities)
            {
                entity.UpdatedDate = now;
            }

            dbSet.UpdateRange(entities);
        }

        public TEntity Find(object id) => dbSet.Find(id);

        public IQueryable<TEntity> Filter(Expression<Func<TEntity, bool>> expression) => dbSet.Where(expression).AsNoTracking();

        public IQueryable<TEntity> SelectAll() => dbSet.AsQueryable<TEntity>();
    }
}
