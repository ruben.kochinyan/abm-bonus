﻿using ABM.Bonus.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace ABM.Bonus.DAL.Configurations
{
    internal class TransactionFactConfiguration : IEntityTypeConfiguration<TransactionFact>
    {
        public void Configure(EntityTypeBuilder<TransactionFact> builder)
        {
            builder.HasKey(k => k.TransactionId);
            builder.Property(p => p.CardBarCode)
                .IsRequired()
                .HasMaxLength(250);
            builder.Property(p => p.PartnerId)
                .IsRequired();
            builder.Property(p => p.UserId)
                .IsRequired();
            builder.Property(p => p.Bonus)
                .IsRequired();
            builder.Property(p => p.TransactionType)
                .IsRequired();

            builder.Property(p => p.CreatedDate)
               .IsRequired();
            builder.Property(p => p.UpdatedDate)
                .IsRequired();
        }
    }
}
