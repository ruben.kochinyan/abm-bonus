﻿using ABM.Bonus.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace ABM.Bonus.DAL.Configurations
{
    internal class UserConfiguration : IEntityTypeConfiguration<User>
    {
        public void Configure(EntityTypeBuilder<User> builder)
        {
            builder.HasKey(k => k.UserId);
            builder.Property(p => p.FirstName)
                .IsRequired()
                .HasMaxLength(250);
            builder.Property(p => p.LastName)
                .IsRequired()
                .HasMaxLength(250);
            builder.Property(p => p.PassportSerialNumber)
                .IsRequired()
                .HasMaxLength(250);
            builder.Property(p => p.PhoneNumber)
                .IsRequired()
                .HasMaxLength(250);
            builder.Property(p => p.BarCode)
                .IsRequired();
            builder.HasOne(c => c.Partner)
               .WithMany(p => p.Users);
            builder.HasOne(u => u.Card)
                .WithOne(c => c.User);

            builder.Property(p => p.CreatedDate)
                .IsRequired();
            builder.Property(p => p.UpdatedDate)
                .IsRequired();

        }
    }
}
