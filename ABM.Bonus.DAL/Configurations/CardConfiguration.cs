﻿using ABM.Bonus.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace ABM.Bonus.DAL.Configurations
{
    internal class CardConfiguration : IEntityTypeConfiguration<Card>
    {
        public void Configure(EntityTypeBuilder<Card> builder)
        {
            builder.HasKey(k => k.BarCode);
            builder.Property(p => p.BarCode)
                .HasMaxLength(50);
            builder.Property(p => p.Bonus)
                .IsRequired();
            builder.Property(p => p.ExpireDate)
                .IsRequired(false);
            builder.HasOne(c => c.Partner)
                .WithMany(p => p.Cards);
            builder.HasOne(c => c.User)
                .WithOne(u => u.Card);

            builder.Property(p => p.CreatedDate)
                .IsRequired();
            builder.Property(p => p.UpdatedDate)
                .IsRequired();
        }
    }
}
