﻿using ABM.Bonus.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace ABM.Bonus.DAL.Configurations
{
    internal class PartnerConfiguration : IEntityTypeConfiguration<Partner>
    {
        public void Configure(EntityTypeBuilder<Partner> builder)
        {
            builder.HasKey(k => k.PartnerId);
            builder.Property(p => p.Email)
                .IsRequired()
                .HasMaxLength(250);
            builder.Property(p => p.Name)
                .IsRequired()
                .HasMaxLength(250);
            builder.Property(p => p.PhoneNumber)
                .IsRequired()
                .HasMaxLength(250);
            builder.HasMany(p => p.Cards)
                .WithOne(c => c.Partner);
            builder.HasMany(p => p.Users)
                .WithOne(u => u.Partner);

            builder.Property(p => p.CreatedDate)
                .IsRequired();
            builder.Property(p => p.UpdatedDate)
                .IsRequired();
        }
    }
}
