﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ABM.Bonus.DAL.Migrations
{
    public partial class Update_TransactionFact_Entity : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "TransactionType",
                table: "TransactionFacts",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "TransactionType",
                table: "TransactionFacts");
        }
    }
}
