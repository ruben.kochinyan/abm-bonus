﻿using System.Threading.Tasks;

namespace ABM.Bonus.DAL
{
    public interface IUnitOfWork
    {
        void SaveChanges();
        Task SavaChangesAsync();
    }
}
