﻿using ABM.Bonus.Entities;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using System.Linq.Expressions;

namespace ABM.Bonus.DAL
{
    public interface IRepository<TEntity> where TEntity : EntityBase
    {
        void Add(TEntity entity);
        Task AddAsync(TEntity entity);
        void AddRange(IEnumerable<TEntity> entities);
        Task AddRangeAsync(IEnumerable<TEntity> entities);
        void Update(TEntity entity);
        void UpdateRange(IEnumerable<TEntity> entities);
        TEntity Find(object id);
        IQueryable<TEntity> Filter(Expression<Func<TEntity, bool>> expression);
        IQueryable<TEntity> SelectAll();

    }
}
