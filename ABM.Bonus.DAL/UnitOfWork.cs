﻿using System.Threading.Tasks;

namespace ABM.Bonus.DAL
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly BonusDbContext dbContext;

        public UnitOfWork(BonusDbContext dbContext)
        {
            this.dbContext = dbContext;
        }

        public void SaveChanges() => dbContext.SaveChanges();

        public async Task SavaChangesAsync() => await dbContext.SaveChangesAsync();
    }
}
