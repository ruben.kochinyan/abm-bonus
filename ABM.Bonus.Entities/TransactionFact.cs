﻿using ABM.Bonus.Enums;

namespace ABM.Bonus.Entities
{
    public class TransactionFact : EntityBase
    {
        public int TransactionId { get; set; }
        public int PartnerId { get; set; }
        public int UserId { get; set; }
        public string CardBarCode { get; set; }
        public decimal Bonus { get; set; }
        public TransactionType TransactionType { get; set; }
    }
}
