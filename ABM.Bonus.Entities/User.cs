﻿namespace ABM.Bonus.Entities
{
    public class User : EntityRoot
    {
        public int UserId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PhoneNumber { get; set; }
        public string PassportSerialNumber { get; set; }
        public string BarCode { get; set; }
        public virtual Card Card { get; set; }
    }
}
