﻿using System;

namespace ABM.Bonus.Entities
{
    public class Card : EntityRoot
    {
        public string BarCode { get; set; }
        public decimal Bonus { get; set; }
        public DateTime? ExpireDate { get; set; }
        public int? UserId { get; set; }
        public virtual User User { get; set; }
    }
}
