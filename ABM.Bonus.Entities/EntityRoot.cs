﻿namespace ABM.Bonus.Entities
{
    public abstract class EntityRoot : EntityBase
    {
        public int PartnerId { get; set; }
        public virtual Partner Partner { get; set; }
    }
}
