﻿using System.Collections.Generic;

namespace ABM.Bonus.Entities
{
    public class Partner : EntityBase
    {
        public int PartnerId { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public virtual ICollection<Card> Cards { get; set; } = new List<Card>();
        public virtual ICollection<User> Users { get; set; } = new List<User>();
    }
}
