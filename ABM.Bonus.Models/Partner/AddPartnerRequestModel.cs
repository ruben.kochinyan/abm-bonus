﻿namespace ABM.Bonus.Models.Partner
{
    public class AddPartnerRequestModel
    {
        public string Name { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
    }
}
