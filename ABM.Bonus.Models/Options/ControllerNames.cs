﻿namespace ABM.Bonus.Models.Options
{
    public class ControllerNames
    {
        public string Bonus { get; set; }
        public string Card { get; set; }
        public string Partner { get; set; }
        public string Transaction { get; set; }
    }
}
