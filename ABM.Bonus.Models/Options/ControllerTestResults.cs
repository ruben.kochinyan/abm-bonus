﻿namespace ABM.Bonus.Models.Options
{
    public class ControllerTestResults
    {
        public string SuccessResultWithoutModel { get; set; }
        public string BonusAddInvalidResult { get; set; }
        public string BonusOutInvalidResult { get; set; }
        public string BonusOutNotEnoughBalanceResult { get; set; }
        public string CardAddInvalidResult { get; set; }
        public string CardAssignInvalidResult { get; set; }
        public string CardGetSuccessResult { get; set; }
        public string CardGetInvalidResult { get; set; }
        public string PartnerAddInvalidResult { get; set; }
        public string TransactionGetByBarCodeInvalidResult { get; set; }
        public string SuccessEmptyResultForModels { get; set; }
        public string SuccessEmptyResultForModel { get; set; }
    }
}
