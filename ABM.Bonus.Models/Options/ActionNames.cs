﻿namespace ABM.Bonus.Models.Options
{
    public class ActionNames
    {
        public string Add { get; set; }
        public string Out { get; set; }
        public string Assign { get; set; }
        public string Get { get; set; }
        public string GetByUserId { get; set; }
        public string GetByBarCode { get; set; }
        public string Filter { get; set; }
    }
}
