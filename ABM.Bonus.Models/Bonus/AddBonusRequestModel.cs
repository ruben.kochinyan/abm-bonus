﻿namespace ABM.Bonus.Models.Bonus
{
    public class AddBonusRequestModel
    {
        public string BarCode { get; set; }
        public decimal Bonus { get; set; }
    }
}
