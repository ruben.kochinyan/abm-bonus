﻿namespace ABM.Bonus.Models.Bonus
{
    public class BonusOutRequestModel
    {
        public string BarCode { get; set; }
        public decimal Bonus { get; set; }
    }
}
