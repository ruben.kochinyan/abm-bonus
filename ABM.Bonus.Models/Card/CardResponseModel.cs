﻿using System;

namespace ABM.Bonus.Models.Card
{
    public class CardResponseModel
    {
        public decimal Bonus { get; set; }
        public DateTime? ExpireDate { get; set; }
        public int UserId { get; set; }
    }
}
