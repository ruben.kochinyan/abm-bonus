﻿using System;

namespace ABM.Bonus.Models.Card
{
    public class AddCardRequestModel
    {
        public string BarCode { get; set; }
        public decimal Bonus { get; set; }
        public DateTime? ExpireDate { get; set; }
    }
}
