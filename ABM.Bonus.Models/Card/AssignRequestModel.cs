﻿namespace ABM.Bonus.Models.Card
{
    public class AssignRequestModel
    {
        public string BarCode { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PhoneNumber { get; set; }
        public string PassportSerialNumber { get; set; }
    }
}
