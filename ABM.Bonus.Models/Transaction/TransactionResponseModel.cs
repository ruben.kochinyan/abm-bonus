﻿using System;

namespace ABM.Bonus.Models.Transaction
{
    public class TransactionResponseModel
    {
        public int TransactionId { get; set; }
        public int UserId { get; set; }
        public string CardBarCode { get; set; }
        public DateTime CreatedDate { get; set; }
    }
}
