﻿using System;

namespace ABM.Bonus.Models.Transaction
{
    public class TransactionFilterRequestModel
    {
        public int? UserId { get; set; }
        public DateTime? From { get; set; }
        public DateTime? To { get; set; }
        public DateTime? ConcreteDay { get; set; }
        public decimal? BonusFrom { get; set; }
        public decimal? BonusTo { get; set; }
    }
}
