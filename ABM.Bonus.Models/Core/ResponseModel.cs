﻿using System.Net;

namespace ABM.Bonus.Models.Core
{
    public abstract class ResponseModel<TData>
    {
        public bool Success { get; set; } = true;
        public HttpStatusCode StatusCode { get; set; } = HttpStatusCode.OK;
        public TData Data { get; set; }
    }
}
