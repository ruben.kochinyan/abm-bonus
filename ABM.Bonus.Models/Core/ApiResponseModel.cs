﻿namespace ABM.Bonus.Models.Core
{
    public sealed class ApiResponseModel<TData> : ResponseModel<TData>
    {
        public ApiResponseModel(TData data)
        {
            Data = data;
        }
    }
}
