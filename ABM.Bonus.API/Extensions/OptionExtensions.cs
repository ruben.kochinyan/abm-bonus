﻿using ABM.Bonus.Models.Options;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace ABM.Bonus.API.Extensions
{
    internal static class OptionExtensions
    {
        internal static void ConfigureInternalOptions(this IServiceCollection services, IConfiguration configuration)
        {
            services.Configure<ControllerTestResults>(options => configuration.GetSection("ControllerTestResults").Bind(options));
            services.Configure<UrlForTests>(options => configuration.GetSection("UrlForTests").Bind(options));
            services.Configure<ControllerNames>(options => configuration.GetSection("ControllerNames").Bind(options));
            services.Configure<ActionNames>(options => configuration.GetSection("ActionNames").Bind(options));
        }
    }
}
