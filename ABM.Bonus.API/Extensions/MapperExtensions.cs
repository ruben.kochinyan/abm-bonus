﻿using AutoMapper;
using Microsoft.Extensions.DependencyInjection;
using System.Reflection;

namespace ABM.Bonus.API.Extensions
{
    internal static class MapperExtensions
    {
        internal static void ConfigureMapper(this IServiceCollection services)
        {
            services.AddAutoMapper(new Assembly[] { typeof(Mappers.AssemblyReference).Assembly });
        }
    }
}
