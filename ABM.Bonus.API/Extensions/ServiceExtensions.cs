﻿using ABM.Bonus.DAL;
using ABM.Bonus.Services.Classes;
using ABM.Bonus.Services.Interfaces;
using Microsoft.Extensions.DependencyInjection;

namespace ABM.Bonus.API.Extensions
{
    internal static class ServiceExtensions
    {
        internal static void ConfigureServices(this IServiceCollection services)
        {
            services.AddScoped(typeof(IRepository<>), typeof(Repository<>));
            services.AddScoped(typeof(IService<>), typeof(Service<>));
            services.AddScoped<IUnitOfWork, UnitOfWork>();
            services.AddScoped<IConfigurationService, ConfigurationService>();
            services.AddScoped<ICardService, CardService>();
            services.AddScoped<IUserService, UserService>();
            services.AddScoped<IPartnerService, PartnerService>();
            services.AddScoped<ITransactionService, TransactionService>();
        }
    }
}
