﻿using ABM.Bonus.DAL;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace ABM.Bonus.API.Extensions
{
    internal static class DbContextExtensions
    {
        internal static void ConfigureDb(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddDbContext<BonusDbContext>(opts =>
                opts.UseSqlServer(configuration.GetConnectionString("DefaultConnection")));
        }
    }
}
