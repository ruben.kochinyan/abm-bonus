﻿using FluentValidation.AspNetCore;
using Microsoft.Extensions.DependencyInjection;

namespace ABM.Bonus.API.Extensions
{
    internal static class ApiExtensions
    {
        internal static void ConfigureApi(this IServiceCollection services)
        {
            services.AddControllers()
                .AddFluentValidation(options =>
                {
                    options.RunDefaultMvcValidationAfterFluentValidationExecutes = false;
                    options.LocalizationEnabled = false;
                    options.RegisterValidatorsFromAssemblyContaining(typeof(Validators.AssemblyReference));
                });
        }
    }
}
