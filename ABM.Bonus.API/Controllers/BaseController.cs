﻿using ABM.Bonus.DAL;
using ABM.Bonus.Models.Core;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ABM.Bonus.API.Controllers
{
    [Route("api/[controller]/[action]")]
    public abstract class BaseController : Controller
    {
        private readonly ILogger<BaseController> logger;
        private readonly IUnitOfWork unitOfWork;
        protected readonly IMapper mapper;
        private readonly IHttpContextAccessor httpContextAccessor;

        protected ApiContext ApiContext { get; }

        public BaseController(ILogger<BaseController> logger,
            IUnitOfWork unitOfWork,
            IMapper mapper,
            IHttpContextAccessor httpContextAccessor)
        {
            this.logger = logger;
            this.unitOfWork = unitOfWork;
            this.mapper = mapper;
            this.httpContextAccessor = httpContextAccessor;
            this.ApiContext = new ApiContext(httpContextAccessor.HttpContext);
        }

        protected void Save() => unitOfWork.SaveChanges();

        protected async Task SaveAsync() => await unitOfWork.SavaChangesAsync();

        protected OkObjectResult ApiResult<TModel>(TModel model = default(TModel)) => new OkObjectResult(new ApiResponseModel<TModel>(model));

        protected OkObjectResult ApiResult<TModel>() => ApiResult<object>();

        protected OkObjectResult Error(ErrorResponseModel model) => new OkObjectResult(model);

        protected OkObjectResult Error(ModelStateDictionary model)
        {
            List<Error> translatedErrors = new List<Error>();
            model.ToList()
                .ForEach(kvp =>
                translatedErrors.AddRange(kvp.Value.Errors.Select(me =>
                new Error("Error", me.ErrorMessage))));

            return Error(new ErrorResponseModel(translatedErrors));
        }

        protected OkObjectResult Error(string key, string message) => Error(new ErrorResponseModel(key, message));

        protected OkObjectResult Error() => Error(ControllerContext.ModelState);

        protected OkObjectResult HandleException(Exception e)
        {
            logger.Log(LogLevel.Critical, e.Message, e.Data);
            return Error("Exception", e.Message);
        }
    }

    public class ApiContext
    {
        private readonly HttpContext httpContext;

        public ApiContext(HttpContext httpContext)
        {
            this.httpContext = httpContext;
        }

        public int UserId => int.Parse(httpContext.User.Claims.Single(x => x.Type == "UserId").Value);
    }
}
