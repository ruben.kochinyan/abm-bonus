﻿using ABM.Bonus.DAL;
using ABM.Bonus.Entities;
using ABM.Bonus.Models.Transaction;
using ABM.Bonus.Services.Interfaces;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ABM.Bonus.API.Controllers
{
    public class TransactionController : BaseController
    {
        private readonly ITransactionService transactionService;

        public TransactionController(ILogger<BaseController> logger,
            IUnitOfWork unitOfWork,
            IMapper mapper,
            IHttpContextAccessor httpContextAccessor,
            ITransactionService transactionService
            ) : base(logger, unitOfWork, mapper, httpContextAccessor)
        {
            this.transactionService = transactionService;
        }

        [HttpGet]
        public async Task<IActionResult> GetByUserId([FromQuery] int userId)
        {
            try
            {
                IEnumerable<TransactionFact> transactions = await transactionService.GetByUserIdAsync(userId);
                IEnumerable<TransactionResponseModel> responseModels = mapper.Map<IEnumerable<TransactionResponseModel>>(transactions);

                return ApiResult(responseModels);
            }
            catch (Exception e)
            {
                return HandleException(e);
            }
        }

        [HttpGet]
        public async Task<IActionResult> GetByBarCode([FromQuery] string bareCode)
        {
            if (string.IsNullOrEmpty(bareCode))
                return Error("Error", "barCode can not be null or empty!");

            try
            {
                IEnumerable<TransactionFact> transactions = await transactionService.GetByBarCodeAsync(bareCode);
                IEnumerable<TransactionResponseModel> responseModels = mapper.Map<IEnumerable<TransactionResponseModel>>(transactions);

                return ApiResult(responseModels);
            }
            catch (Exception e)
            {
                return HandleException(e);
            }
        }

        [HttpPost]
        public async Task<IActionResult> Filter([FromBody] TransactionFilterRequestModel model)
        {
            if (!ModelState.IsValid)
                return Error(ModelState);

            try
            {
                IEnumerable<TransactionFact> transactions = await transactionService.Filter(model);
                IEnumerable<TransactionResponseModel> responseModels = mapper.Map<IEnumerable<TransactionResponseModel>>(transactions);

                return ApiResult(responseModels);
            }
            catch (Exception e)
            {
                return HandleException(e);
            }
        }
    }
}
