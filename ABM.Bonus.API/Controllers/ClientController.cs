﻿using ABM.Bonus.DAL;
using ABM.Bonus.Models;
using ABM.Bonus.Models.Bonus;
using ABM.Bonus.Models.Card;
using ABM.Bonus.Models.Core;
using ABM.Bonus.Models.Options;
using ABM.Bonus.Models.Partner;
using ABM.Bonus.Models.Transaction;
using ABM.Bonus.Services.Interfaces;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace ABM.Bonus.API.Controllers
{
    public class ClientController : BaseController
    {
        private readonly IHttpClientFactory httpClientFactory;
        private readonly IConfigurationService configurationService;
        private string Url { get; set; }
        private ControllerNames ControllerName { get; }
        private ActionNames ActioNName { get; }

        public ClientController(ILogger<ClientController> logger,
            IUnitOfWork unitOfWork,
            IMapper mapper,
            IHttpContextAccessor httpContextAccessor,
            IHttpClientFactory httpClientFactory,
            IConfigurationService configurationService
            ) : base(logger, unitOfWork, mapper, httpContextAccessor)
        {
            this.httpClientFactory = httpClientFactory;
            this.configurationService = configurationService;
            Url = configurationService.UrlForTestsConfiguuration().Url;
            ControllerName = configurationService.ControllerNamesConfiguration();
            ActioNName = configurationService.ActionNamesConfiguration();
        }

        [HttpGet]
        public async Task<IActionResult> Get()
        {
            var client = httpClientFactory.CreateClient();

            using (var response = await client.GetAsync("http://localhost:52650/api/Test/Get"))
            {
                var result = response.Content.ReadAsStringAsync();
            }

            return Ok();
        }

        [HttpPost]
        public async Task<IActionResult> Post()
        {

            var client = httpClientFactory.CreateClient();
            var requestModel = new AssignRequestModel
            {
                BarCode = string.Empty,
                FirstName = string.Empty,
                LastName = "sads",
                PassportSerialNumber = string.Empty,
                PhoneNumber = "sdsd5454"
            };

            using (var response = await client.PostAsync($"{Url}Card/Assign", new StringContent(JsonConvert.SerializeObject(requestModel), Encoding.UTF8, "application/json")))
            {
                var result = response.Content.ReadAsStringAsync();

                var test = configurationService.ControllerTestResultsConfiguration();
                //if (test.TestModelResult.Contains(result.Result))
                //{
                //    return Ok();
                //}
            }

            return Ok();
        }

        [HttpPost]
        public async Task<IActionResult> BonusAdd()
        {
            var client = httpClientFactory.CreateClient();


            using (var response = await client.GetAsync($"{Url}{configurationService.ControllerNamesConfiguration().Card}/{configurationService.ActionNamesConfiguration().GetByUserId}"))//, new StringContent(JsonConvert.SerializeObject(requestModel), Encoding.UTF8, "application/json")))
            {
                var result = response.Content.ReadAsStringAsync();
                var test = configurationService.ControllerTestResultsConfiguration().SuccessEmptyResultForModels;

                if (test.Contains(result.Result))
                {
                    return Ok();
                }
            }

            return Ok();
        }
    }
}
