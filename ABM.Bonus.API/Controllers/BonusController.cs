﻿using ABM.Bonus.DAL;
using ABM.Bonus.Entities;
using ABM.Bonus.Enums;
using ABM.Bonus.Models.Bonus;
using ABM.Bonus.Services.Interfaces;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ABM.Bonus.API.Controllers
{
    public class BonusController : BaseController
    {
        private readonly ICardService cardService;
        private readonly ITransactionService transactionService;

        public BonusController(ILogger<BaseController> logger,
            IUnitOfWork unitOfWork,
            IMapper mapper,
            IHttpContextAccessor httpContextAccessor,
            ICardService cardService,
            ITransactionService transactionService
            ) : base(logger, unitOfWork, mapper, httpContextAccessor)
        {
            this.cardService = cardService;
            this.transactionService = transactionService;
        }

        [HttpPost]
        public async Task<IActionResult> Add([FromBody] AddBonusRequestModel model)
        {
            if (!ModelState.IsValid)
                return Error(ModelState);

            try
            {
                Card card = cardService.Find(model.BarCode);
                card.Bonus += model.Bonus;

                cardService.Update(card);
                await transactionService.Insert(card, model.Bonus, TransactionType.Added);
                await SaveAsync();
                
                return ApiResult(true);
            }
            catch (Exception e)
            {
                return HandleException(e);
            }
        }

        [HttpPost]
        public async Task<IActionResult> Out([FromBody] BonusOutRequestModel model)
        {
            if (!ModelState.IsValid)
                return Error(ModelState);

            try
            {
                Card card = cardService.Find(model.BarCode);
                if (card.Bonus < model.Bonus)
                    return Error("Error", "Not enough balance!");

                card.Bonus -= model.Bonus;
                //TODO: insert in transaction
                cardService.Update(card);
                await SaveAsync();

                return ApiResult(true);
            }
            catch (Exception e)
            {
                return HandleException(e);
            }
        }
    }
}
