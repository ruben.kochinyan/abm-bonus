﻿using ABM.Bonus.DAL;
using ABM.Bonus.Entities;
using ABM.Bonus.Models.Partner;
using ABM.Bonus.Services.Interfaces;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;

namespace ABM.Bonus.API.Controllers
{
    public class PartnerController : BaseController
    {
        private readonly IPartnerService partnerService;

        public PartnerController(ILogger<BaseController> logger,
            IUnitOfWork unitOfWork, 
            IMapper mapper,
            IHttpContextAccessor httpContextAccessor,
            IPartnerService partnerService
            ) : base(logger, unitOfWork, mapper, httpContextAccessor)
        {
            this.partnerService = partnerService;
        }

        [HttpPost]
        public async Task<IActionResult> Add([FromBody] AddPartnerRequestModel model)
        {
            if (!ModelState.IsValid)
                return Error(ModelState);

            try
            {
                Partner partner = mapper.Map<Partner>(model);
                await partnerService.AddAsync(partner);
                await SaveAsync();

                return ApiResult(true);
            }
            catch (Exception e)
            {
                return HandleException(e);
            }
        }
    }
}
