﻿using ABM.Bonus.DAL;
using ABM.Bonus.Entities;
using ABM.Bonus.Models.Card;
using ABM.Bonus.Services.Interfaces;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;

namespace ABM.Bonus.API.Controllers
{
    public class CardController : BaseController
    {
        private readonly ICardService cardService;
        private readonly IUserService userService;

        public CardController(ILogger<BaseController> logger, 
            IUnitOfWork unitOfWork,
            IMapper mapper,
            IHttpContextAccessor httpContextAccessor,
            ICardService cardService,
            IUserService userService
            ) : base(logger, unitOfWork, mapper, httpContextAccessor)
        {
            this.cardService = cardService;
            this.userService = userService;
        }

        [HttpPost]
        public async Task<IActionResult> Add([FromBody] AddCardRequestModel model)
        {
            if (!ModelState.IsValid)
                return Error(ModelState);

            try
            {
                Card card = mapper.Map<Card>(model);
                card.PartnerId = 1; //temp
                await cardService.AddAsync(card);
                await SaveAsync();

                return ApiResult(true);
            }
            catch (Exception e)
            {
                return HandleException(e);
            }
        }

        [HttpPost]
        public async Task<IActionResult> Assign([FromBody] AssignRequestModel model)
        {
            if (!ModelState.IsValid)
                return Error(ModelState);

            try
            {
                User user = mapper.Map<User>(model);
                user.PartnerId = 1; //temp
                await userService.AddAsync(user);

                Card card = cardService.Find(model.BarCode);
                card.PartnerId = 1; //temp
                card.UserId = user.UserId;
                card.User = user;

                cardService.Update(card);
                await SaveAsync();

                return ApiResult(true);
            }
            catch (Exception e)
            {
                return HandleException(e);
            }
        }

        [HttpGet]
        public async Task<IActionResult> Get([FromQuery] string barCode)
        {
            if (string.IsNullOrEmpty(barCode))
                return Error("Error", "barCode can not be null or empty!");

            try
            {
                Card card = await cardService.GetByBarCodeAsync(barCode);
                CardResponseModel responseModel = mapper.Map<CardResponseModel>(card);

                return ApiResult(responseModel);
            }
            catch (Exception e)
            {
                return HandleException(e);
            }
        }

        [Authorize]
        [HttpGet]
        public async Task<IActionResult> GetByUserId()
        {
            try
            {
                Card card = await cardService.GetByUserIdAsync(ApiContext.UserId);
                CardResponseModel responseModel = mapper.Map<CardResponseModel>(card);

                return ApiResult(responseModel);
            }
            catch (Exception e)
            {
                return HandleException(e);
            }
        }
    }
}
