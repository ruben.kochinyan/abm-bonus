﻿using ABM.Bonus.Entities;
using ABM.Bonus.Models.Card;
using AutoMapper;

namespace ABM.Bonus.Mappers
{
    public class CardMapper : Profile
    {
        public CardMapper()
        {
            CreateMap<AddCardRequestModel, Card>();
            CreateMap<Card, CardResponseModel>();
        }
    }
}
