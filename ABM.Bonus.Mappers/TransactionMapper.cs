﻿using ABM.Bonus.Entities;
using ABM.Bonus.Models.Transaction;
using AutoMapper;

namespace ABM.Bonus.Mappers
{
    public class TransactionMapper : Profile
    {
        public TransactionMapper()
        {
            CreateMap<TransactionFact, TransactionResponseModel>();
        }
    }
}
