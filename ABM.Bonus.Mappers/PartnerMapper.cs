﻿using ABM.Bonus.Entities;
using ABM.Bonus.Models.Partner;
using AutoMapper;

namespace ABM.Bonus.Mappers
{
    public class PartnerMapper : Profile
    {
        public PartnerMapper()
        {
            CreateMap<AddPartnerRequestModel, Partner>();
        }
    }
}
