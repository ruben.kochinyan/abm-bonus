﻿using ABM.Bonus.Entities;
using ABM.Bonus.Models.Card;
using AutoMapper;

namespace ABM.Bonus.Mappers
{
    public class UserMapper : Profile
    {
        public UserMapper()
        {
            CreateMap< AssignRequestModel, User>();
        }
    }
}
