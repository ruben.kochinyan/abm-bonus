﻿using ABM.Bonus.Models;
using FluentValidation;

namespace ABM.Bonus.Validators
{
    public class TestRequestModelValidaroe : AbstractValidator<TestRequestModel>
    {
        public TestRequestModelValidaroe()
        {
            RuleFor(p => p.Name)
                .NotEmpty()
                .NotNull();
        }
    }
}
