﻿using ABM.Bonus.Models.Card;
using FluentValidation;

namespace ABM.Bonus.Validators.Card
{
    public class AddCardRequestModelValidator : AbstractValidator<AddCardRequestModel>
    {
        public AddCardRequestModelValidator()
        {
            RuleFor(p => p.BarCode)
                .NotEmpty()
                .NotNull();
            RuleFor(p => p.Bonus)
                .NotEmpty()
                .NotNull();
        }
    }
}
