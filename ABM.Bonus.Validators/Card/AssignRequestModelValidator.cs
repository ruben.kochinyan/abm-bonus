﻿using ABM.Bonus.Models.Card;
using FluentValidation;

namespace ABM.Bonus.Validators.Card
{
    public class AssignRequestModelValidator : AbstractValidator<AssignRequestModel>
    {
        public AssignRequestModelValidator()
        {
            RuleFor(p => p.BarCode)
                .NotEmpty()
                .NotNull();
            RuleFor(p => p.FirstName)
                .NotEmpty()
                .NotNull()
                .MaximumLength(250);
            RuleFor(p => p.LastName)
                .NotEmpty()
                .NotNull()
                .MaximumLength(250);
            RuleFor(p => p.PassportSerialNumber)
                .NotEmpty()
                .NotNull()
                .MaximumLength(250);
            RuleFor(p => p.PhoneNumber)
                .NotEmpty()
                .NotNull()
                .MaximumLength(250);
        }
    }
}
