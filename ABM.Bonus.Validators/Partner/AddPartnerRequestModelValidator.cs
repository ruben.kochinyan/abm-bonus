﻿using ABM.Bonus.Models.Partner;
using FluentValidation;

namespace ABM.Bonus.Validators.Partner
{
    public class AddPartnerRequestModelValidator : AbstractValidator<AddPartnerRequestModel>
    {
        public AddPartnerRequestModelValidator()
        {
            RuleFor(p => p.Email)
                .EmailAddress();
            RuleFor(p => p.Name)
                .NotEmpty()
                .NotNull()
                .MaximumLength(250);
            RuleFor(p => p.PhoneNumber)
                .NotEmpty()
                .NotNull()
                .MaximumLength(250);
        }
    }
}
