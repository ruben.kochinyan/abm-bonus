﻿using ABM.Bonus.Models.Bonus;
using FluentValidation;

namespace ABM.Bonus.Validators.Bonus
{
    public class AddBonusRequestModelValidator : AbstractValidator<AddBonusRequestModel>
    {
        public AddBonusRequestModelValidator()
        {
            RuleFor(p => p.BarCode)
                .NotEmpty()
                .NotNull();
            RuleFor(p => p.Bonus)
                .NotEmpty()
                .NotNull();
        }
    }
}
