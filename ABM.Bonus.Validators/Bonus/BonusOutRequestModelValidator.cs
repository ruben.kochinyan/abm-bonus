﻿using ABM.Bonus.Models.Bonus;
using FluentValidation;

namespace ABM.Bonus.Validators.Bonus
{
    public class BonusOutRequestModelValidator : AbstractValidator<BonusOutRequestModel>
    {
        public BonusOutRequestModelValidator()
        {
            RuleFor(p => p.BarCode)
                .NotEmpty()
                .NotNull();
            RuleFor(p => p.Bonus)
                .NotEmpty()
                .NotNull();
        }
    }
}
